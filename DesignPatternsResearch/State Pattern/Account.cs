﻿using System;

namespace DesignPatternsResearch
{
	class Account
	{
		private State _state;

		// Constructor
		public Account()
		{
			// New accounts are 'Silver' by default
			_state = new SilverState(0.0, this);
		}

		// Properties
		public double Balance
		{
			get { return _state.Balance; }
		}

		public State State
		{
			get { return _state; }
			set { _state = value; }
		}

		public void Deposit(double amount)
		{
			_state.Deposit(amount);
			Console.WriteLine("Deposited {0:C} --- ", amount);
			Console.WriteLine(" Balance = {0:C}", Balance);
			Console.WriteLine(" Status = {0}", State.GetType().Name);
			Console.WriteLine("");
		}

		public void Withdraw(double amount)
		{
			_state.Withdraw(amount);
			Console.WriteLine("Withdrew {0:C} --- ", amount);
			Console.WriteLine(" Balance = {0:C}", Balance);
			Console.WriteLine(" Status = {0}\n", State.GetType().Name);
		}

		public void PayInterest()
		{
			_state.PayInterest();
			Console.WriteLine("Interest Paid at {0:P} --- ", State.Interest);
			Console.WriteLine(" Balance = {0:C}", Balance);
			Console.WriteLine(" Status = {0}\n", State.GetType().Name);
		}
	}
}
