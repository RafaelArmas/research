﻿namespace DesignPatternsResearch
{
	public class LegalCover : Decorator
	{
		public LegalCover(BaseProduct product) : base(product)
		{
			Price = 70;
		}
	}
}
