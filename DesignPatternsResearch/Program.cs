﻿using System;
using System.Threading.Tasks;

namespace DesignPatternsResearch
{
	class Program
	{
		static void Main(string[] args)
		{
			//TestCascadeLambdaPatternExample();
			//TestDecoratorPattern();
			//TestRetryPattern();
			TestStatePattern();
			Console.ReadLine();
		}

		private static void TestCascadeLambdaPatternExample()
		{
			// The action specified by the delegate as an argument to the Send() method clearly indicates that the action is related to constructing an email.
			MailManager.Send(manager => manager
				.To("yourcaseonline.test@gmail.com")
				.From("kaizen.text@minsterlaw.co.uk")
				.Subject("Test email")
				.Body("This is only a test, please ignore."));
		}

		private static void TestDecoratorPattern()
		{
			var carInsurance = new CarInsurance();
			Console.WriteLine("Car insurance costs: " + carInsurance.GetPrice());
			var carInsuranceWithNoExcess = new NoExcess(carInsurance);
			Console.WriteLine("Car insurance with no excess costs: " + carInsuranceWithNoExcess.GetPrice());
			var carInsuranceWithNoExcessAndLegalCover = new LegalCover(carInsuranceWithNoExcess);
			Console.WriteLine("Car insurance with no excess and legal cover costs: " + carInsuranceWithNoExcessAndLegalCover.GetPrice());
			Console.WriteLine("\n");
			var bikeInsurance = new BikeInsurance();
			Console.WriteLine("Bike insurance costs: " + bikeInsurance.GetPrice());
			var bikeInsuranceWithNoExcess = new NoExcess(bikeInsurance);
			Console.WriteLine("Bike insurance with no excess costs: " + bikeInsuranceWithNoExcess.GetPrice());
			var bikeInsuranceWithNoExcessAndLegalCover = new LegalCover(bikeInsuranceWithNoExcess);
			Console.WriteLine("Bike insurance with no excess and legal cover costs: " + bikeInsuranceWithNoExcessAndLegalCover.GetPrice());
		}

		private static void TestRetryPattern()
		{
			Task.Run(() => SmsManager.SendSmsWithRetry("+447770530722", "Test message, please ignore."));
		}

		private static void TestStatePattern()
		{
			// Open a new account
			var account = new Account();

			// Apply financial transactions
			account.Deposit(500.0);
			account.Deposit(300.0);
			account.PayInterest();
			account.Deposit(300.0);
			account.PayInterest();
			account.Withdraw(2000.00);
			account.Withdraw(1100.00);
			account.PayInterest();
		}
	}
}
