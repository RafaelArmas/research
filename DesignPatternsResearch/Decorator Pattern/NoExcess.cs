﻿namespace DesignPatternsResearch
{
	public class NoExcess : Decorator
	{
		public NoExcess(BaseProduct product) : base(product)
		{
			Price = 100;
		}
	}
}
