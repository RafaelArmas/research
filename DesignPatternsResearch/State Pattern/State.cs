﻿namespace DesignPatternsResearch
{
	abstract class State
	{
		protected double LowerLimit;
		protected double UpperLimit;

		// Properties
		public Account Account { get; set; }
		public double Balance { get; set; }
		public double Interest { get; set; }

		public abstract void Deposit(double amount);
		public abstract void Withdraw(double amount);
		public abstract void PayInterest();
	}
}
