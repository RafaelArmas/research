﻿namespace DesignPatternsResearch
{
	public abstract class Decorator : BaseProduct
	{
		protected BaseProduct Product;

		public Decorator(BaseProduct product)
		{
			Product = product;
		}

		public override double GetPrice()
		{
			return Product.GetPrice() + Price;
		}
	}
}
