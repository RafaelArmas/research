﻿using System;
using System.Threading.Tasks;
using Twilio;
using Twilio.Exceptions;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace DesignPatternsResearch
{
	public class SmsManager
	{
		private const int RetryCount = 3;
		private const int DelayBetweenRetries = 10000;
		private const string Sid = "wrong";// "ACdd92fb953316fa15021c440423086fff";
		private const string Token = "9663a7b7603f71505bc7b5f40ef0d93c";
		private const string From = "+447481342598";

		public static async Task SendSmsWithRetry(string recipient, string message)
		{
			int currentRetry = 0;

			for (;;)
			{
				try
				{
					await SendSmsMessage(recipient, message);
					Console.WriteLine("Message sent successfully.");
					break;
				}
				catch (Exception ex)
				{
					Console.WriteLine(ex.ToString());
					currentRetry++;

					if (currentRetry > RetryCount || !IsTransient(ex))
					{
						// We should add message to a poison queue and/or alert someone.
						throw;
					}
				}
				// Consider exponential delay best suited for operation.
				await Task.Delay(DelayBetweenRetries);
			}
		}

		public static Task SendSmsMessage(string recipient, string message)
		{
			TwilioClient.Init(Sid, Token);
			var response = MessageResource.Create(new PhoneNumber(recipient), from: new PhoneNumber(From), body: message);
			return Task.FromResult(response);
		}

		private static bool IsTransient(Exception ex)
		{
			if (ex is ApiException)
				return true;

			// Additional exception checking goes here.
			return false;
		}
	}
}
