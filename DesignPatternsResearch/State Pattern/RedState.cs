﻿using System;

namespace DesignPatternsResearch
{
	class RedState : State
	{
		// Constructor
		public RedState(State state)
		{
			Balance = state.Balance;
			Account = state.Account;
			Initialize();
		}

		private void Initialize()
		{
			// Should come from a datasource
			Interest = 0.0;
			LowerLimit = -100.0;
			UpperLimit = 0.0;
		}

		public override void Deposit(double amount)
		{
			Balance += amount;
			StateChangeCheck();
		}

		public override void Withdraw(double amount)
		{
			Console.WriteLine("No funds available for withdrawal!");
		}

		public override void PayInterest()
		{
			Console.WriteLine("No interest is paid!");
		}

		private void StateChangeCheck()
		{
			if (Balance > UpperLimit)
			{
				Account.State = new SilverState(this);
			}
		}
	}
}
