﻿using System;
using System.Net.Mail;

namespace DesignPatternsResearch
{
	public class MailManager
	{
		private static string _toAddress;
		private static string _fromAddress;
		private static string _subject;
		private static string _body;

		public MailManager To(string address)
		{
			_toAddress = address;
			return this;
		}

		public MailManager From(string address)
		{
			_fromAddress = address;
			return this;
		}
		public MailManager Subject(string subject)
		{
			_subject = subject;
			return this;
		}
		public MailManager Body(string body)
		{
			_body = body;
			return this;
		}

		public static void Send(Action<MailManager> action)
		{
			action(new MailManager());
			var message = new MailMessage(_fromAddress, _toAddress, _subject, _body);
			var client = new SmtpClient("yk-p-exch-01");
			client.Send(message);
			Console.WriteLine("Email has been sent.");
		}
	}
}
